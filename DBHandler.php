<?php

include_once 'config.php';

class DBHandler
{
	public $mysqli;
	
	function __construct()
	{
		$this->mysqli = new mysqli(HOST, USER, PASSWORD, DATABASE);
		
		if ($this->mysqli->connect_errno)
		{
			echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
			die();
		}
	}
	
	function __destructor()
	{
		mysqli_close($mysqli);
	}
	
	public function checkExistence($url)
	{
		$mysqli = $this->mysqli;
	
		$result = mysqli_query($mysqli, "SELECT id, short FROM " . TABLENAME . " WHERE url LIKE '$url'") 
					or $this->DBError(__FILE__, __LINE__, mysqli_error($mysqli));
	
		if (mysqli_num_rows($result) > 0)
		{
			return mysqli_fetch_row($result);
		}
		else
		{
			return false;
		}
	}
	
	public function DBaddURL($url, $short)
	{
		$mysqli = $this->mysqli;
		
		mysqli_query($mysqli, "INSERT INTO " . TABLENAME . " (url, short) VALUES ('$url', '$short')")
		or $this->DBError(__FILE__, __LINE__, mysqli_error($mysqli));
	}
	
	private function DBError($file, $line, $error)
	{
		die("File: $file<br>
			Line: $line<br>
			Error: $error"); 
	}
	
}

?>