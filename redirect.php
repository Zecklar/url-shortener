<?php
	include_once 'DBHandler.php';
	
	$db = new DBHandler();
	$mysqli = $db->mysqli;
	
	$short = mysqli_real_escape_string($mysqli, trim($_GET['short']) );
	
	$result = mysqli_query($mysqli, "SELECT * FROM " . TABLENAME . " WHERE short='" . $short . "'") 
				or db_Error(__FILE__, __LINE__, mysqli_error($mysqli));
				
	$row = mysqli_fetch_row($result);
	
	if(!empty($row))
	{
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: " . $row[1]);
	}
	else
	{
		header("HTTP/1.0 404 Not Found");	
	}
?>