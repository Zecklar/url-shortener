<?php

include_once 'DBHandler.php';

// Protocols
define("PROTOCOLS", "http|https|ftp|ftps");

class URLShortener
{
	// Handler for MYSQL database
	private $dbhandler;
	
	// Array for errors
	private $ERRORS = array();
	
	// Constructor
	function __construct()
	{
		$this->dbhandler = new DBHandler();
	}
	
	function __destructor()
	{
		
	}
	
	// The main function
	public function shorten()
	{
		if(count($_POST) > 0)
		{
			$mysqli = $this->dbhandler->mysqli;
			$url = mysqli_real_escape_string($mysqli, trim($_POST['url']) );
			
			$url = $this->checkValidation($url);
			
			
			if(empty($this->ERRORS))
			{
				if( ($data = $this->dbhandler->checkExistence($url)) )
				{
					$short = $data[1];
				}
				else
				{
					$short = $this->genShortURL();
					$this->dbhandler->DBaddURL($url, $short);
				}
				
				return $_SERVER['SERVER_NAME'] . "/" . $short;
			}
			else
			{
				$this->printErrors();
			}
		}
	}
	
	// Generate the short code for the URL
	private function genShortURL()
	{
		$mysqli = $this->dbhandler->mysqli;
		
		// Get the row number
		$result = mysqli_query($mysqli,("SELECT COUNT(*) FROM " . TABLENAME))
					or db_Error(__FILE, __LINE__, musqli_error($mysqli));
					
		$rows = mysqli_fetch_row($result);
		$num = $rows[0];
		
		mysqli_free_result($result);
		
		$chars = "abcdefghjkmnpqrstuvwxyz23456789ABCDEFGHJKMNPQRSTUVWXYZ";
		$length = strlen($chars);
		
		// Convert to array
		$chars = str_split($chars);
		
		$shortUrl = "";
		
		// Adding letters backwards from the charlist
		while($num > ($length - 1) )
		{
			$index = $num % $length;
			$num = floor($num / $length) - 1;
			$shortUrl = $chars[$index] . $shortUrl;
		}
		
		return $chars[$num] . $shortUrl;
	}
	
	// Check if the input is validate
	private function checkValidation($url)
	{
		if (empty($url))
		{
			$this->ERRORS[] = "Enter a URL.";
			return;
		}
	
		if(!preg_match("/^(".PROTOCOLS.")\:\/\//i", $url))
		{
			$url = "http://" . $url;	
		}
		
		$url_info = parse_url($url);
		
		// Check if the site exists and is up
		if(strlen($url_info['host']) == 0 || !$this->siteStatus($url))
		{
			$this->ERRORS[] = "Enter a valid URL.";
			return;
		}
		
		return $url;
	}
	
	// Checks that the site is fine
	private function siteStatus($url)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch,  CURLOPT_RETURNTRANSFER, TRUE);
		
		$response = curl_exec($ch);
		$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		
		$c_error = curl_error($ch);
		curl_close($ch);
		
		// Site is not found or curl couldn't get the info
		if($status == '404' || !empty($c_error))
		{
			return false;
		}
		else
		{
			return true;	
		}
	}
	
	// For custom printing errors in HTML document
	public function getErrors()
	{
		return $this->ERRORS;
	}
	
	// Echoes the errors as a list
	public function printErrors()
	{
		$error = $this->ERRORS;
		
		if (count($error) > 0)
		{
			echo "<ul>\n";
			
			foreach ($error as $key => $value)
			{
				echo "<li>$value</li>\n";
			}
			
			echo "</ul>\n";
		}
	}
}
?> 