<?php
	include_once  'shortener.php';
	
	// Change this to 0 after development
	ini_set('display_errors', '1');
	
	$shortener = new URLShortener();
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>URL Shortener</title>

<style>
	h1
	{
		text-align: center;
		font-size: 30px;	
	}

	#container
	{
		width: 500px;
		font-family: Arial, Helvetica, sans-serif;
		margin: auto;
	}
	
	#form, #print_errors
	{
		width: 350px;
		margin: auto;
		text-align: center;
	}
	
	#print_errors li
	{
		text-align: center;
		list-style-type: none;	
	}
	
</style>

</head>

<body>

	<div id="container">
    
    	<h1>URL Shortener</h1>
    	
        <div id="form">
            <form method="POST" action="">
                <label>Enter URL: </label><input type="text" name="url" />
              <input type="submit" value="Shorten">
            </form>
        </div>
		
        </br>
		
        <div id="print_errors">
			<?php
				$shortener->printErrors();
				echo $shortener->shorten();
			?>
        </div>
        
    </div>

</body>
</html>