# README #

# Requirements #

* MySQL v5.6
* PHP 5.4

# Installation #

1. Upload the files
2. Set up your database with a table name of your choice. The table must contain columns called '**id**', '**url**' and '**short**' because the queries are sent with these column names.
3. Configure **config.php**. Add your user login credentials which are used to establish the database connection. You need to add correct database and table names also.